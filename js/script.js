class Hamburger {
    constructor (size, stuffing){
        try{
            try {
                let isSize = Object.getOwnPropertyNames(Hamburger.SIZES).some(function (item) {
                    return item === size;
                });
                let isStuffing = Object.getOwnPropertyNames(Hamburger.STUFFINGS).some(function (item) {
                    return item === stuffing;
                });
                if (!arguments.length) throw new HamburgerException ('no size given');
                else if (!isSize) throw new HamburgerException ('invalid size ' + size);
                else if (!isStuffing) throw new HamburgerException ('invalid stuffing ' + stuffing);
                if (isSize && isStuffing){
                    this.size = size;
                    this.staffing = stuffing;
                    this.toppings = [];
                }
            }
            catch (e) {
                console.log(e.message);
            }
        }
        catch (e) {
            console.log(e.message);
        }
    }
    addTopping(topping) {
        if (isEmpty(this.getToppings(), topping)) {
            return this.getToppings().push(topping);
        } else try {
            throw new HamburgerException ('duplicate topping ' + topping);
        }
        catch (e) {
            console.log(e.message);
        }
    }
    removeTopping (topping) {
        if (this.toppings.includes(topping)) {
            return this.toppings.splice(this.toppings.indexOf(topping),1);
        }
    }
    getToppings () {
        return this.toppings;
    }
    getSize () {
        return this.size;
    }
    getStuffing () {
        return this.staffing;
    }
    calculatePrice () {
        return Hamburger.SIZES[this.getSize()].price + Hamburger.STUFFINGS[this.getStuffing()].price + this.getToppings().reduce(function (sum, current) {
            return sum + Hamburger.TOPPINGS[current].price;
        }, 0);
    };
    calculateCalories () {
        return Hamburger.SIZES[this.getSize()].calories + Hamburger.STUFFINGS[this.getStuffing()].calories + this.getToppings().reduce(function (sum, current) {
            return sum + Hamburger.TOPPINGS[current].calories;
        }, 0);
    };

}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL] : {
        price: 50,
        calories : 20
    },
    [Hamburger.SIZE_LARGE] : {
        price: 100,
        calories : 40
    }
};
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE] : {
        price: 10,
        calories : 20
    },
    [Hamburger.STUFFING_SALAD] : {
        price: 20,
        calories : 5
    },
    [Hamburger.STUFFING_POTATO] : {
        price: 15,
        calories : 10
    }
};
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.TOPPINGS ={
    [Hamburger.TOPPING_MAYO] : {
        price: 20,
        calories : 5
    },
    [Hamburger.TOPPING_SPICE] : {
        price: 15,
        calories : 0
    }
};

function isEmpty(obj, keyObj) {
    for (let key in obj) {
        if (obj[key] === keyObj) return false;
    }
    return true;
}

function HamburgerException (message) {
    this.message = message;
    this.stack = (new Error()).stack;
}

HamburgerException.prototype = Object.create(Error.prototype);

let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(`Calories ${hamburger.calculateCalories()}`);
console.log(`Price: ${hamburger.calculatePrice()}`);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(`Price with sauce: ${hamburger.calculatePrice()}`);
console.log(`Is hamburger large: ${hamburger.getSize() === Hamburger.SIZE_LARGE}`);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`Have ${hamburger.getToppings().length} toppings`);
console.log(`Have ${hamburger.getStuffing()} stuffings`);
console.log(`Have ${hamburger.getToppings()} toppings`);

const h2 = new Hamburger();
const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);

